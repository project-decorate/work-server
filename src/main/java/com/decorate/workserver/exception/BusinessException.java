package com.decorate.workserver.exception;

/**
 * @Title : 业务异常
 * @Author : lcf
 * @Date : 2020/5/18
*/
public class BusinessException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String errorCode ="";
    private String errorDesc ="";

    public BusinessException() {
        super();
    }

    public BusinessException(Throwable t) {
        super(t);
    }

    public BusinessException(String code, String message, Throwable t) {
        super(t);
        this.errorCode = code;
        this.errorDesc = message;
    }

    public BusinessException(String code, String message) {
        this(code, message, new Exception(message));
    }

    public BusinessException(String message) {
        this("", message);
    }

    public BusinessException(String message, Throwable t) {
        this("", message, t);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }
    
    @Override
    public String toString() {
    	return "errorCode:" + this.errorCode + ";errorDesc:" + this.errorDesc;
    }

}
