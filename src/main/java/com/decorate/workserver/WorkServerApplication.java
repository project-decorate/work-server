package com.decorate.workserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.decorate.workserver.mapper")
@SpringBootApplication
public class WorkServerApplication extends ServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(WorkServerApplication.class, args);
	}

}
