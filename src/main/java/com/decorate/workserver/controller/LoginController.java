package com.decorate.workserver.controller;

import com.alibaba.fastjson.JSONObject;
import com.decorate.workserver.base.Response;
import com.decorate.workserver.domain.vo.LoginVo;
import com.decorate.workserver.utils.CommonUtils;
import com.decorate.workserver.utils.ConstantsUtil;
import com.decorate.workserver.utils.redis.RedisManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 登录控制层
 * @date : 2020/11/10
 */
@RestController
public class LoginController extends BaseController{
    /** logger */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    /** 
     * 登录
     * @date : 2020/11/10
    */
    @PostMapping("/login")
    @ResponseBody
    public Response login(@RequestBody Map<String, Object> paramMap) {
        try {
            LOGGER.info("用户登录参数:{}", JSONObject.toJSON(paramMap));
            String mobile = CommonUtils.getValue(paramMap,"mobile");
            // TODO 1、用户登录校验，2、用户信息保存，3、token存储
            LoginVo loginVo = new LoginVo();
            String token = genetateToken();
            loginVo.setToken(token);
            RedisManager.getRedis().set(ConstantsUtil.CacheKey.LOGIN_TOKEN+token,loginVo);
            RedisManager.getRedis().set(ConstantsUtil.CacheKey.LOGIN_MOBILE+mobile,token);
            LOGGER.info("用户登录结果:{}", JSONObject.toJSON(loginVo));
            return super.succResponse(loginVo);
//        } catch (BusinessException be) {
//            be.printStackTrace();
//            return businessFail(be.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            return failResponse(e.getMessage());
        }

    }

    /** 
     * 退出
     * @date : 2020/11/10
    */
    @GetMapping("/logout")
    @ResponseBody
    public Response logout() {
        try {
            LOGGER.info("用户退出");
            LoginVo loginInfo = getLoginInfo();
            if(loginInfo!=null){
                RedisManager.getRedis().delKey(ConstantsUtil.CacheKey.LOGIN_TOKEN+getTokenId());
                RedisManager.getRedis().delKey(ConstantsUtil.CacheKey.LOGIN_MOBILE+loginInfo.getMobile());
            }
            return Response.successResponse();
//        } catch (BusinessException be) {
//            be.printStackTrace();
//            return busFailResponse(be.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            return failResponse(e.getMessage());
        }

    }
}
