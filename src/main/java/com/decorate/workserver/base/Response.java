package com.decorate.workserver.base;


import com.decorate.workserver.utils.enums.RequestCodeEnums;

public class Response {
    private String resCode;
    private String resMsg;
    private Object data;

    public Response() {
		super();
	}

	public Response(String resCode, String resMsg, Object data) {
		super();
		this.resCode = resCode;
		this.resMsg = resMsg;
		this.data = data;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResMsg() {
		return resMsg;
	}

	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	/**
     * 获取数据成功
     * **/
    public static Response successResponse(Object data) {
        return new Response(RequestCodeEnums.SUCCESS.getCode(), RequestCodeEnums.SUCCESS.getMsg(), data);
    }

    public static Response successResponse() {
        return new Response(RequestCodeEnums.SUCCESS.getCode(), RequestCodeEnums.SUCCESS.getMsg(), "");
    }

    public static Response response(String bankCardNo, String resMsg, Object data) {
        return new Response(bankCardNo, resMsg, data);
    }

    public static Response response(String bankCardNo) {
        return new Response(bankCardNo, "", "");
    }

    public static Response response(String bankCardNo, String resMsg) {
        return new Response(bankCardNo, resMsg, "");
    }

    public static Response fail(String resMsg) {
        return new Response(RequestCodeEnums.FAILED.getCode(), resMsg, "");
    }
    public static Response businessFail(String resMsg) {
        return new Response(RequestCodeEnums.BUSINESS_FAILED.getCode(), resMsg, "");
    }

    public static Response success(String resMsg) {
        return new Response(RequestCodeEnums.SUCCESS.getCode(), resMsg, "");
    }

    public static Response fail(String resMsg, Object data) {
        return new Response(RequestCodeEnums.FAILED.getCode(), resMsg, data);
    }

    public static Response success(String resMsg, Object data) {
        return new Response(RequestCodeEnums.SUCCESS.getCode(), resMsg, data);
    }
    
    public static Response loginDate(String resMsg, Object data) {
    	return new Response(RequestCodeEnums.LOGIN_EXPIRE.getCode(), resMsg, data);
    }

}
