package com.decorate.workserver.utils.redis;

import com.decorate.workserver.utils.ToolSpring;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * redis
 * @date : 2020/11/10
*/
public class RedisManager {

    @Autowired
    private RedisClient redisClient;

    private RedisClient getRedisClient() {
        if (null == this.redisClient) {
            this.redisClient = (RedisClient) ToolSpring.getBean("RedisClient");
        }
        return redisClient;
    }

    private static RedisManager instance = null;

    static {
        instance = new RedisManager();
    }

    private RedisManager() {}
    private static RedisManager getInstance() {
        return instance;
    }
    public static RedisClient getRedis(){
        return getInstance().getRedisClient();
    }

}
