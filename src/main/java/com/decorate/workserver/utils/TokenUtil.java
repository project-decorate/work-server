package com.decorate.workserver.utils;

import java.util.UUID;

/**
 * @title : token工具
 * @date : 2020/11/10
 */
public class TokenUtil {
    public static String genetateToken() {
        return UUID.randomUUID().toString();
    }
}
