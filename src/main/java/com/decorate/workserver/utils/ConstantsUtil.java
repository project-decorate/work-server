package com.decorate.workserver.utils;

/**
 * 工具类
 */
public class ConstantsUtil {

    /**
     * header
     */
    public static class Header {
        public static final String token = "x-auth-token"; // 特别提示函
        public static final String accessToken = "accessToken"; // 参数中accessToken
    }

    /**
     * 缓存KEY
     */
    public static class CacheKey {
        public static final String LOGIN_MOBILE = "SERVERLOGIN:MOBILE:";
        public static final String LOGIN_TOKEN = "SERVERLOGIN:TOKEN:";
    }

}
