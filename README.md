# work-server

#### 介绍
server端代码

#### 软件架构
spring boot

#### maven打包
1.  默认启动使用的是dev配置
2.  打包生产：clean package -P prd
3.  打包测试：clean package -P test，需先创建application-test.properties文件
